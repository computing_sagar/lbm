//
//  Solver.hpp
//  LBM
//
//  Created by Sagar Dolas on 30/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Solver_hpp
#define Solver_hpp

#include <stdio.h>
#include <memory>
#include <iostream>
#include <cmath>

#include "Lattice.hpp"
#include "Stencil.hpp"
#include "Type.h"

#include <omp.h>

namespace LBM  {
    
    class Solver {
        
    private:
        
        std::shared_ptr<Lattice> src ;
        std::shared_ptr<Lattice> dst ;
        std::shared_ptr<Lattice> velocity ;
        std::shared_ptr<Lattice> density ;
        const std::shared_ptr<Lattice> boolarray ;
        
        real_t omega ;
        u_int cellx ;
        u_int celly ;
        real_t ax ;
        real_t ay ;
        
    public:
        
        explicit Solver(const real_t nu_L,const real_t ax, const real_t ay,std::shared_ptr<Lattice> _src ,std::shared_ptr<Lattice> _dst,std::shared_ptr<Lattice> vel ,std::shared_ptr<Lattice> den,const std::shared_ptr<Lattice> _boolarray) ;
        ~Solver() ;
        
        // Solver Methods for LBM
        void BoundaryHandlingPeriodic() ;
        void BoundaryHandlingReflective() ;
        void Initialise() ;
        void Stream() ;
        void StreamCollide() ; 
        void collide() ;
        void ObstacleSweep() ;
        void SwapPointers() ;
        void IndentifyObstacle(const real_t dx,const real_t obsrad) ;
        void ObsTreatment() ;
        const real_t distbw(const real_t x1, const real_t y1,const real_t x2, const real_t y2) ;
    };
}

#endif /* Solver_hpp */
