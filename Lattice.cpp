//
//  Lattice.cpp
//  LBM
//
//  Created by Sagar Dolas on 28/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include "Lattice.hpp"


LBM::Lattice::Lattice(const u_int _numcellx,const u_int _numcelly,const LBM::LatticeVariables var) : numcellx(_numcellx),numcelly(_numcelly), numvelocities(var){

    //Total number of cells
    u_int totcells = numcellx * numcelly ;
    u_int memory = totcells * numvelocities ;
    data.resize(memory,0.0) ;
    
}

LBM::Lattice::Lattice(const Lattice & lat) : numcellx(lat.numcellx),numcelly(lat.numcelly),numvelocities(lat.numvelocities) {
    
    data.resize(lat.size(), 0.0) ;
    for (u_int i =0; i < lat.size(); ++i) {
        data[i] = lat.data[i] ;
    }
    
}

LBM::Lattice& LBM::Lattice::operator=(const LBM::Lattice &lat) noexcept{
    
    for (u_int i =0; i < lat.size(); ++i) {
        data[i] = lat.data[i] ;
    }
    return (*this) ;

}

LBM::Lattice::~Lattice(){
    
}


const u_int LBM::Lattice::NumCellsinXdir() const {
    
    return numcellx ;
    
}

const u_int LBM::Lattice::NumcellsinYdir() const {
    
    return numcelly ;
    
}

const u_int LBM::Lattice::totalNumCells() const {
    
    return  numcellx * numcelly ;
    
}

const ul_int LBM::Lattice::size() const {
    
    return data.size() ;
    
}






