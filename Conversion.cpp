//
//  Conversion.cpp
//  LBM
//
//  Created by Sagar Dolas on 30/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include "Conversion.hpp"

LBM::Conversion::Conversion(const real_t dx, const real_t dt) : dx(dx),dt(dt) {
    
}

LBM::Conversion::~Conversion(){
    
}

const real_t LBM::Conversion::getViscosityinLU(const real_t real_mu) {
    
    return  (real_mu * dt / (dx * dx)) ;
}

const real_t LBM::Conversion::getViscosityinPhyU(const real_t LU_mu){
    
    return  (LU_mu * dx * dx / dt) ;
}

const real_t LBM::Conversion::getAccinLU(const real_t a){
    
    return (a * dt * dt / dx) ;
}

