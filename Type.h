//
//  Type.h
//  LBM
//
//  Created by Sagar Dolas on 28/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Type_h
#define Type_h

typedef double real_t ;
typedef unsigned u_int ;
typedef unsigned long ul_int;

#endif /* Type_h */
