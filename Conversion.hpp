//
//  Conversion.hpp
//  LBM
//
//  Created by Sagar Dolas on 30/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Conversion_hpp
#define Conversion_hpp

#include <stdio.h>
#include "Lattice.hpp"
#include "Enums.h"
#include "Type.h"

namespace LBM  {
    
    class Conversion {
        
    private:
        
        real_t dx ;
        real_t dt ;
        
    public:
        
        // Constructor
        explicit Conversion(const real_t dx,const real_t dt) ;
        ~Conversion() ;
        
        // Conversion functions
        //void getVelocityinPhyU(const Lattice & realvel) ;
        const real_t getViscosityinLU(const real_t real_mu) ;
        const real_t getViscosityinPhyU(const real_t LU_mu) ;
        
        const real_t getAccinLU(const real_t a) ; 
        
    };
}

#endif /* Conversion_hpp */
