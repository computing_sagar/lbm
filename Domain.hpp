//
//  Domain.hpp
//  LBM
//
//  Created by Sagar Dolas on 29/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Domain_hpp
#define Domain_hpp

#include "Type.h"

#include <stdio.h>

namespace LBM  {
    class Domain{
        
    private:
        
        real_t dx ;
        real_t dt ;
        real_t xlength ;
        real_t ylength ;
        
    public:
        // Constructor and Destructor
        Domain(const real_t _dx , const real_t _dt ,const real_t _xlength, const real_t _ylength) ;
        ~Domain() ;
        
        // Domain Parameters
        const u_int numcellx() const ;
        const u_int numcelly() const ;
        const u_int totalnumcell() const ;
        
        // Lattice Parameters
        const real_t DX() const ;
        const real_t DT() const ;
        const u_int xlength_L() const ;
        const u_int ylength_L() const ;
        
    };
}


#endif /* Domain_hpp */
