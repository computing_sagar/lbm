//
//  ProcessData.hpp
//  LBM
//
//  Created by Sagar Dolas on 04/07/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef ProcessData_hpp
#define ProcessData_hpp

#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <string>

#include "GrayScaleImage.h"
#include "lodepng.h"
#include "Lattice.hpp"
#include "Stencil.hpp"


namespace LBM {
    
    class Image {
        
    public:
        // Constructor
        Image() ;
        ~Image() ;
        
        // Generate Image
        void GenerateImage(const Lattice &velocity,const Lattice &obs_cell,const std::string filename) ;
        
    };
}


#endif /* ProcessData_hpp */
