//
//  Stencil.hpp
//  LBM
//
//  Created by Sagar Dolas on 28/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Stencil_hpp
#define Stencil_hpp

#include <stdio.h>

#include"Type.h"


namespace LBM {
    
    namespace Stencil {
        
        class Direction {
            
        public:
            
            static const u_int C = 0 ;
            static const u_int E = 1 ;
            static const u_int N = 2 ;
            static const u_int W = 3 ;
            static const u_int S = 4 ;
            static const u_int NE = 5 ;
            static const u_int NW = 6 ;
            static const u_int SW = 7 ;
            static const u_int SE = 8 ;
            
            Direction() {} ;
            ~Direction() {} ;
            
        };
        
        class Weight {
            
        public:
            constexpr static const real_t C = 4.0 / 9.0 ;
            constexpr static const real_t S = 1.0 / 9.0 ;
            constexpr static const real_t D = 1.0 / 36.0;
        };
        
        class vector{
            
        public:
            constexpr static const u_int x = 0 ;
            constexpr static const u_int y = 1 ; 
            
        };
    }
}

#endif /* Stencil_hpp */
