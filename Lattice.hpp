//
//  Lattice.hpp
//  LBM
//
//  Created by Sagar Dolas on 28/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Lattice_hpp
#define Lattice_hpp

#include <stdio.h>
#include <vector>

#include "Type.h"
#include "Enums.h"
#include "Stencil.hpp"


// This is D2Q19 lattice class .

namespace LBM {
    
    class Lattice {
    
    private:
        
        u_int numcellx ;
        u_int numcelly ;
        u_int numvelocities ;
        std::vector<real_t> data ;
        
    public:
        
        // Constructor
        Lattice(const u_int _numcellx ,const u_int _numcelly,const LBM::LatticeVariables var) ;
        Lattice(const Lattice & lat) ;
        
        // Copy Assignment Operator
        Lattice & operator = (const Lattice & lat) noexcept ;
        
        // Destructor
        ~Lattice() ;
        
        // Access Operator
        inline const real_t& get(const u_int x,const u_int y,const u_int direction) const {
            
            return data[ numvelocities * (y * numcellx + x) + direction ] ;
        }
        
        inline real_t& get(const u_int x, const u_int y, const u_int direction) {
            
            return data[ numvelocities * (y * numcellx + x) + direction ] ;
        }
        
        // Information
        const u_int NumCellsinXdir() const ;
        const u_int NumcellsinYdir() const ;
        const u_int totalNumCells()  const ;
        const ul_int size() const ;
        
        
        
        
    };
    
}

#endif /* Lattice_hpp */
