//
//  main.cpp
//  LBM
//
//  Created by Sagar Dolas on 28/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include <iostream>
#include <memory>
#include <tuple>

#include "Stencil.hpp"
#include "Domain.hpp"
#include "Lattice.hpp"
#include "Enums.h"
#include "Conversion.hpp"
#include "Solver.hpp"
#include "ProcessData.hpp"
#include "ProcessData.cpp"

using namespace LBM ;
using namespace LBM::Stencil ;

int main(int argc, const char * argv[]) {
    
    
    // Default Simulation Parameters all parameters in MKS system
    u_int obstacle_res = 30 ;
    real_t acc_x = 0.01 ;
    real_t nu = 1e-06 ;
    real_t obstacle_dia = 0.005 ;
    real_t obstacle_rad = 0.0025 ;
    real_t dt = 0.0008	 ;
    real_t time_end = 3.0 ;
    std::string filename = "scenario1.png" ;
    
    //domain desciption
    real_t xlength = 0.06 ;
    real_t ylength = 0.02 ;
    
    // Scenario Name
    std:: string scene(argv[1]) ;
    if (scene == "scenario2") {
        obstacle_res = 60 ;
        acc_x = 0.016 ;
        dt = 0.0001 ;
        time_end = 5 ;
        filename = "scenario2.png" ;
    }

    // LBM  Parameters
    const real_t dx = obstacle_dia / obstacle_res ;
    
    //Conversion to lattice units
    Conversion convert(dx,dt) ;
    real_t nu_l = convert.getViscosityinLU(nu) ;
    real_t ax_l = convert.getAccinLU(acc_x) ;
    real_t ay_l = 0.0 ;
    real_t tot_iter = time_end / dt ;
    
    std::cout<<"The dx is :="<<dx<<std::endl ;
    std::cout<<"The dt is :="<<dt<<std::endl ;
    std::cout<<"The obstacle Diameter is :="<<obstacle_dia<<std::endl ;
    std::cout<<"The obstacle resolution is :="<<obstacle_res<<std::endl ;
    std::cout<<"The Total number of iteration are :="<<tot_iter<<std::endl ;
    std::cout<<"The viscosity in lattice units :="<<nu_l<<std::endl ;
    std::cout<<"The acceleration in Lattice units :="<<ax_l<<std::endl ;
    
    // Forming the domain
    LBM::Domain rectangle(dx,dt,xlength,ylength) ;

    //Lattice pointer
    std::shared_ptr<Lattice> D2Q9src  = std::make_shared<Lattice>(rectangle.numcellx(),rectangle.numcelly(),LatticeVariables::discretization) ;
    std::shared_ptr<Lattice> D2Q9dst  = std::make_shared<Lattice>(rectangle.numcellx(),rectangle.numcelly(),LatticeVariables::discretization) ;
    std::shared_ptr<Lattice> velocity = std::make_shared<Lattice>(rectangle.numcellx(),rectangle.numcelly(),LatticeVariables::velocities );
    std::shared_ptr<Lattice> Density  = std::make_shared<Lattice>(rectangle.numcellx(),rectangle.numcelly(),LatticeVariables::density) ;
    std::shared_ptr<Lattice> boolarray = std::make_shared<Lattice>(rectangle.numcellx(),rectangle.numcelly(),LatticeVariables::density) ;

    
    // Constructing the solver object
    Solver solve(nu_l, ax_l, ay_l, D2Q9src, D2Q9dst, velocity, Density, boolarray) ;
    
    // Starting the solver
    solve.IndentifyObstacle(dx, obstacle_rad) ;
    solve.Initialise() ;
    
    // Solver Loop
    for (u_int iter =0; iter < tot_iter; ++iter) {
    
        solve.BoundaryHandlingPeriodic();
        solve.BoundaryHandlingReflective();
        solve.ObsTreatment() ;
        solve.StreamCollide() ;
        solve.SwapPointers();
        if (iter %100 ==0){
        	//std::cout<<iter<<std::endl ;
        }
    }
    
    // Generating the image
    LBM::Image velimage ;
    velimage.GenerateImage(*velocity,*boolarray,filename) ;

    return 0;

}
