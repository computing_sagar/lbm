#Declaring the variables
CC=g++ 

#Declaring the flags
CFLAGS = -c -std=c++11 -ffast-math -O3 -fopenmp -DNDEBUG -Wall -pedantic
FLAGS  = -std=c++11 -ffast-math -O3 -fopenmp -DNEBUG -Wall -pedantic

#Declaring the ColSamm Folder 
lib    = Source/ 		

all 				:LBM 

LBM				:Domain.o Lattice.o Simulation.o Solver.o Conversion.o GrayScaleImage.o lodepng.o
				$(CC) $(FLAGS) Domain.o Lattice.o Solver.o Conversion.o GrayScaleImage.o lodepng.o Simulation.o -o lbm

Domain.o			:Domain.cpp
				$(CC) $(CFLAGS) Domain.cpp

Lattice.o			:Lattice.cpp
				$(CC) $(CFLAGS) Lattice.cpp

Simulation.o			:Simulation.cpp
				$(CC) $(CFLAGS) Simulation.cpp

Solver.o			:Solver.cpp
				$(CC) $(CFLAGS) Solver.cpp

Conversion.o			:Conversion.cpp
				$(CC) $(CFLAGS) Conversion.cpp

GrayScaleImage.o 		:GrayScaleImage.o
				$(CC) $(CFLAGS) GrayScaleImage.cpp

lodepng.o			:lodepng.o
				$(CC) $(CFLAGS) lodepng.cpp

clean				:
				rm -rf *.o lbm

		
