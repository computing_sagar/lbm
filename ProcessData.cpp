//
//  ProcessData.cpp
//  LBM
//
//  Created by Sagar Dolas on 04/07/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include "ProcessData.hpp"

LBM::Image::Image() {
    
    
}

LBM::Image::~Image(){
    
    
}

void LBM::Image::GenerateImage(const LBM::Lattice &velocity,const  LBM::Lattice &obs_cell,const std::string filename)
{
    
    std::vector<real_t> vel_x ;
    std::vector<int> color ;
    
    u_int cellx  = velocity.NumCellsinXdir() ;
    u_int celly =  velocity.NumcellsinYdir() ;
    
    for (u_int y =0; y < celly; ++y) {
        for (u_int x = 0; x < cellx; ++x) {
            vel_x.push_back(velocity.get(x, y, LBM::Stencil::vector::x)) ;
        }
    }
    
    auto max = std::max_element(vel_x.begin(), vel_x.end());
    auto min = std::min_element(vel_x.begin(), vel_x.end()) ;
    
    if (*max > 0.25) {
        std::cout<<"Results may not be correct , due to high lattice velocities"<<std::endl ;
    }
    
    int p =0.0 ;
    for (u_int v =0 ; v < vel_x.size(); ++v) {
        p = 255 *((vel_x[v]) - ((*min))) / ((*max) -(*min)) ;
        //std::cout<<p<<std::endl ;
        color.push_back(p) ;
    }
    
    GrayScaleImage velimage(cellx, celly) ;
    
    for (u_int y =0; y < celly; ++y) {
        for (u_int x =0 ; x < cellx; ++x) {
            velimage.getElement(x, y) = color[ y * cellx  + x ] ;
        }
    }
    
    std::cout<<"Generating the Image file"<<std::endl ; 
    
    velimage.save(filename) ;

    
}
