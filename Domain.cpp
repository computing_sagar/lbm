//
//  Domain.cpp
//  LBM
//
//  Created by Sagar Dolas on 29/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include "Domain.hpp"


LBM::Domain::Domain(const real_t _dx, const real_t _dt , const real_t _xlength, const real_t _ylength) : dx(_dx),dt(_dt),xlength(_xlength), ylength(_ylength) {
    
    
}

LBM::Domain::~Domain(){
    
}

const u_int LBM::Domain::numcellx() const {
    
    return static_cast<u_int>((xlength/dx) + 2) ;
    
}

const u_int LBM::Domain::numcelly() const {
    
    return static_cast<u_int>((ylength/dx) + 2) ;
}

const u_int LBM::Domain::totalnumcell() const {
    
    return numcellx() * numcelly() ;
    
}

const real_t LBM::Domain::DX() const {
    
    return dx ;
}

const real_t LBM::Domain::DT() const {
    
    return dt ;
}

const u_int LBM::Domain::xlength_L()const{
    
    return static_cast<u_int>(xlength/dx) ;
    
}

const u_int LBM::Domain::ylength_L() const {
    
    return static_cast<u_int>(ylength/dx) ;
}

