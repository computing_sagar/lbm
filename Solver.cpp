//
//  Solver.cpp
//  LBM
//
//  Created by Sagar Dolas on 30/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include "Solver.hpp"

using namespace LBM::Stencil ;

LBM::Solver::Solver(const real_t nu_L,const real_t ax,const real_t ay,std::shared_ptr<Lattice> _src ,std::shared_ptr<Lattice> _dst,std::shared_ptr<Lattice> vel ,std::shared_ptr<Lattice> den,const std::shared_ptr<Lattice> _boolarray) :src(_src),dst(_dst),velocity(vel),density(den),boolarray(_boolarray) ,ax(ax),ay(ay) {
    
    omega = 1.0 / (( 3.0 * nu_L) + 0.5) ;
    
    cellx = src->NumCellsinXdir() ;
    celly = src->NumcellsinYdir() ;
    
    std::cout<<"The value of omega is"<<omega<<std::endl ; 
    
}

LBM::Solver::~Solver(){
    
}

void LBM::Solver::BoundaryHandlingPeriodic(){
    
    u_int x = 0 ;
    for (u_int y =1; y < celly-1; ++y) {
        
        // for the left periodic conditions
        src->get(x, y, Direction::C)  = src->get(cellx-2, y, Direction::C) ;
        src->get(x, y, Direction::E)  = src->get(cellx-2, y, Direction::E) ;
        src->get(x, y, Direction::NE) = src->get(cellx-2, y, Direction::NE) ;
        src->get(x, y, Direction::N)  = src->get(cellx-2, y, Direction::N) ;
        src->get(x, y, Direction::NW) = src->get(cellx-2, y, Direction::NW) ;
        src->get(x, y, Direction::W)  = src->get(cellx-2, y, Direction::W) ;
        src->get(x, y, Direction::SW) = src->get(cellx-2, y, Direction::SW) ;
        src->get(x, y, Direction::S)  = src->get(cellx-2, y, Direction::S) ;
        src->get(x, y, Direction::SE) = src->get(cellx-2, y, Direction::SE) ;
        
    }
    
    x = cellx-1 ;
    for (u_int y = 1; y < celly-1; ++y) {
        
        // for the right periodic conditions
        src->get(x, y, Direction::C)  = src->get(1, y, Direction::C) ;
        src->get(x, y, Direction::E)  = src->get(1, y, Direction::E) ;
        src->get(x, y, Direction::NE) = src->get(1, y, Direction::NE) ;
        src->get(x, y, Direction::N)  = src->get(1, y, Direction::N) ;
        src->get(x, y, Direction::NW) = src->get(1, y, Direction::NW);
        src->get(x, y, Direction::W)  = src->get(1, y, Direction::W) ;
        src->get(x, y, Direction::SW) = src->get(1, y, Direction::SW) ;
        src->get(x, y, Direction::S)  = src->get(1, y, Direction::S) ;
        src->get(x, y, Direction::SE) = src->get(1, y, Direction::SE) ;
        
    }
}

void LBM::Solver::BoundaryHandlingReflective(){
    
    // for all four corners
    src->get(0, 0, Direction::NE) = src->get(1, 1, Direction::SW) ;
    src->get(0, celly-1, Direction::SE) = src->get(1, celly-2, Direction::NW) ;
    src->get(cellx-1, celly-1, Direction::SW) = src->get(cellx-2, celly-2, Direction::NE) ;
    src->get(cellx-1, 0, Direction::NW) = src->get(cellx-2, 1, Direction::SE) ;
    
    // for top cells
    u_int y = celly-1 ;
    for (u_int x = 1; x < cellx-1; ++x) {
        
        src->get(x, y, Direction::SW) = src->get(x-1, y-1, Direction::NE) ;
        src->get(x, y, Direction::S)  = src->get(x, y-1, Direction::N) ;
        src->get(x, y, Direction::SE) = src->get(x+1, y-1, Direction::NW) ;
    
    }
    
    // for bottom cell
    y = 0 ;
    for (u_int x = 1; x < cellx-1; ++x) {
        
        src->get(x, y, Direction::NW) = src->get(x-1, y+1, Direction::SE) ;
        src->get(x, y, Direction::N)  = src->get(x, y+1, Direction::S) ;
        src->get(x, y, Direction::NE) = src->get(x+1, y+1, Direction::SW) ;
        
    
    }
}

void LBM::Solver::Initialise(){
    
    // Traverse Interior Cells
    for (u_int y = 1;y < celly-1 ; ++y) {
        for (u_int x = 1; x < cellx - 1; ++x) {
            if (boolarray->get(x, y, Stencil::vector::x)==0) {
                
                src->get(x, y, Direction::C) = Weight::C ;
                src->get(x, y, Direction::E) = src->get(x, y, Direction::N) = src->get(x, y, Direction::W) = src->get(x, y, Direction::S) = Weight::S ;
                src->get(x, y, Direction::NE) = src->get(x, y, Direction::NW) = src->get(x, y, Direction::SE) = src->get(x, y, Direction::SW) = Weight::D ;
                density->get(x, y, vector::x) = 1.0 ;
            }
        }
    }
}
void LBM::Solver::Stream(){

    // Streaming operation for inner cells
    for (u_int y = 1; y < celly-1; ++y) {
        for (u_int x = 1; x < cellx-1; ++x) {
            if (boolarray->get(x, y, Stencil::vector::x)==0) {
                
                // pull distribution function from all ..sides
                dst->get(x, y, Direction::C)  = src->get(x, y, Direction::C) ;
                dst->get(x, y, Direction::E)  = src->get(x-1, y, Direction::E) ;
                dst->get(x, y, Direction::NE) = src->get(x-1, y-1, Direction::NE) ;
                dst->get(x, y, Direction::N)  = src->get(x, y-1, Direction::N) ;
                dst->get(x, y, Direction::NW) = src->get(x+1, y-1, Direction::NW) ;
                dst->get(x, y, Direction::W)  = src->get(x+1, y, Direction::W) ;
                dst->get(x, y, Direction::SW) = src->get(x+1, y+1, Direction::SW) ;
                dst->get(x, y, Direction::S)  = src->get(x, y+1, Direction::S) ;
                dst->get(x, y, Direction::SE) = src->get(x-1, y+1, Direction::SE) ;
                
            }
        }
    }
}

// Streaming step will be done on destination cell // Stream pull will be the done
void LBM::Solver::StreamCollide(){
    
    // Streaming operation for inner cells
    
    #pragma omp parallel for schedule (static)
    for (u_int y = 1; y < celly-1; ++y) {
        for (u_int x = 1; x < cellx-1; ++x) {
            if (boolarray->get(x, y, Stencil::vector::x)==0) {
            
                // pull distribution function from all ..sides
                dst->get(x, y, Direction::C)  = src->get(x, y, Direction::C) ;
                dst->get(x, y, Direction::E)  = src->get(x-1, y, Direction::E) ;
                dst->get(x, y, Direction::NE) = src->get(x-1, y-1, Direction::NE) ;
                dst->get(x, y, Direction::N)  = src->get(x, y-1, Direction::N) ;
                dst->get(x, y, Direction::NW) = src->get(x+1, y-1, Direction::NW) ;
                dst->get(x, y, Direction::W)  = src->get(x+1, y, Direction::W) ;
                dst->get(x, y, Direction::SW) = src->get(x+1, y+1, Direction::SW) ;
                dst->get(x, y, Direction::S)  = src->get(x, y+1, Direction::S) ;
                dst->get(x, y, Direction::SE) = src->get(x-1, y+1, Direction::SE) ;
                
                real_t rho = 0.0 ;
                real_t velx = 0.0 ;
                real_t vely = 0.0 ;
                
                rho  =  dst->get(x, y, Direction::C)  + dst->get(x, y, Direction::E) +
                dst->get(x, y, Direction::NE) + dst->get(x, y, Direction::N) +
                dst->get(x, y, Direction::NW) + dst->get(x, y, Direction::W) +
                dst->get(x, y, Direction::SW) + dst->get(x, y, Direction::S) +
                dst->get(x, y, Direction::SE) ;
                
                density->get(x, y, vector::x) = rho ;
                const real_t rhoinv = 1.0 /rho ;
                
                velx = ( (dst->get(x, y, Direction::NE) + dst->get(x, y, Direction::E) +
                          dst->get(x, y, Direction::SE)) - ( dst->get(x, y, Direction::NW) +
                                                            dst->get(x, y, Direction::W) + dst->get(x, y, Direction::SW)) ) * rhoinv;
                
                velocity->get(x, y, vector::x) = velx ;
                
                vely =  ((dst->get(x, y, Direction::NW) + dst->get(x, y, Direction::N) + dst->get(x, y,  Direction::NE) ) - (dst->get    (x, y, Direction::SW) + dst->get(x, y, Direction::S) + dst->get(x, y, Direction::SE))) *rhoinv ;
                
                velocity->get(x, y, vector::y) = vely ;
                
                const real_t velxx = velx * velx ;
                const real_t velyy = vely * vely ;
                
                // For the center
                real_t feqC = Stencil::Weight::C * rho * (1.0 - 3.0 * (velxx + velyy) * 0.5 ) ;
                dst->get(x, y, Direction::C) +=  omega * (feqC - dst->get(x, y, Direction::C)) ;
                
                // For the east distribution function
                real_t feqE = Stencil::Weight::S * rho * (1.0 + 3.0 * velx + 4.5 * velxx - 1.5 * (velxx + velyy))  ;
                dst->get(x, y, Direction::E) +=  omega * ( feqE - dst->get(x, y, Direction::E)) + 3.0 * Stencil::Weight::S * rho * ax ;
                
                //for the North East Direction
                real_t feqNE = Stencil::Weight::D * rho * (1.0 + 3.0*(velx + vely) + 4.5 * (velx+vely) * (velx + vely) - 1.5* (velxx+ velyy)) ;
                dst->get(x, y, Direction::NE) += omega * (feqNE - dst->get(x, y, Direction::NE)) + 3.0 * Stencil::Weight::S * rho * (ax + ay) ;
                
                // For the north direction
                real_t feqN = Stencil::Weight::S * rho * (1.0 + 3.0 * vely + 4.5 * velyy - 1.5 * (velxx + velyy) ) ;
                dst->get(x, y, Direction::N) += omega * (feqN - dst->get(x, y, Direction::N)) + 3.0 * Stencil::Weight::S * rho * ay;
                
                // For the North west direction
                real_t feqNW = Stencil::Weight::D * rho * (1.0 + 3.0 * (-velx + vely) + 4.5 * (-velx + vely) * (-velx + vely) - 1.5 * (velxx + velyy) ) ;
                dst->get(x, y, Direction::NW) += omega * (feqNW - dst->get(x, y, Direction::NW)) + 3.0 * Stencil::Weight::S * rho * (-ax + ay);
                
                // for the west direction
                real_t feqW  = Stencil::Weight::S * rho * (1.0 - 3.0 * velx + 4.5 * velxx - 1.5 * (velxx+ velyy)) ;
                dst->get(x, y, Direction::W) += omega * (feqW - dst->get(x, y, Direction::W))+ 3.0 * Stencil::Weight::S * rho * (-ax) ;
                
                // for the south west
                real_t feqSW = Stencil::Weight::D * rho * (1.0 -3.0 * (velx+ vely) + 4.5 * (velx+ vely) * (velx+ vely) - 1.5 * (velxx+ velyy) ) ;
                dst->get(x, y, Direction::SW) += omega * (feqSW - dst->get(x, y, Direction::SW))+ 3.0 * Stencil::Weight::S * rho * (-ax-ay) ;
                
                // for the South direction
                real_t feqS = Stencil::Weight::S * rho * (1.0 -3.0 * vely + 4.5 *velyy - 1.5 * (velxx + velyy) ) ;
                dst->get(x, y, Direction::S) += omega * (feqS  - dst->get(x, y, Direction::S)) + 3.0 * Stencil::Weight::S * rho * (-ay) ;
                
                // for the south east direction
                real_t feqSE = Stencil::Weight::D * rho * (1.0 + 3.0 * (velx - vely) + 4.5 * (velx-vely) * (velx-vely) - 1.5 * (velxx+ velyy) ) ;
                dst->get(x, y, Direction::SE) += omega * (feqSE - dst->get(x, y, Direction::SE) ) + 3.0 * Stencil::Weight::S * rho * (ax-ay)  ;

            }
        }
    }
}

void LBM::Solver::collide(){
    
    // Collision os local to cell operation for all interior cells
    
    // First of all we need to figure out the density of this cell
    real_t rho = 0.0 ;
    real_t velx = 0.0 ;
    real_t vely = 0.0 ;
    
    for (u_int y = 1; y < celly-1; ++y) {
        for (u_int x = 1; x < cellx-1; ++x) {
            
            if (boolarray->get(x, y, Stencil::vector::x)==0) {
            
                rho  =  src->get(x, y, Direction::C)  + src->get(x, y, Direction::E) +
                        src->get(x, y, Direction::NE) + src->get(x, y, Direction::N) +
                        src->get(x, y, Direction::NW) + src->get(x, y, Direction::W) +
                        src->get(x, y, Direction::SW) + src->get(x, y, Direction::S) +
                        src->get(x, y, Direction::SE) ;                
                
                density->get(x, y, vector::x) = rho ;
                
                velx = ( (src->get(x, y, Direction::NE) + src->get(x, y, Direction::E) +
                        src->get(x, y, Direction::SE)) - ( src->get(x, y, Direction::NW) +
                        src->get(x, y, Direction::W) + src->get(x, y, Direction::SW)) ) / rho;
                
                velocity->get(x, y, vector::x) = velx ;
                
                vely =  ((src->get(x, y, Direction::NW) + src->get(x, y, Direction::N) + src->get(x, y,  Direction::NE) ) - (src->get    (x, y, Direction::SW) + src->get(x, y, Direction::S) + src->get(x, y, Direction::SE))) / rho ;
                
                velocity->get(x, y, vector::y) = vely ;
                
                const real_t velxx = velx * velx ;
                const real_t velyy = vely * vely ;
                
                // For the center
                real_t feqC = Stencil::Weight::C * rho * (1.0 - 3.0 * (velxx + velyy) * 0.5 ) ;
                src->get(x, y, Direction::C) +=  omega * (feqC - src->get(x, y, Direction::C)) ;
                
                // For the east distribution function
                real_t feqE = Stencil::Weight::S * rho * (1.0 + 3.0 * velx + 4.5 * velxx - 1.5 * (velxx + velyy))  ;
                src->get(x, y, Direction::E) +=  omega * ( feqE - src->get(x, y, Direction::E)) + 3.0 * Stencil::Weight::S * rho * ax ;
                
                //for the North East Direction
                real_t feqNE = Stencil::Weight::D * rho * (1.0 + 3.0*(velx + vely) + 4.5 * (velx+vely) * (velx + vely) - 1.5* (velxx+ velyy)) ;
                src->get(x, y, Direction::NE) += omega * (feqNE - src->get(x, y, Direction::NE)) + 3.0 * Stencil::Weight::S * rho * (ax + ay) ;
                
                // For the north direction
                real_t feqN = Stencil::Weight::S * rho * (1.0 + 3.0 * vely + 4.5 * velyy - 1.5 * (velxx + velyy) ) ;
                src->get(x, y, Direction::N) += omega * (feqN - src->get(x, y, Direction::N)) + 3.0 * Stencil::Weight::S * rho * ay;
                
                // For the North west direction
                real_t feqNW = Stencil::Weight::D * rho * (1.0 + 3.0 * (-velx + vely) + 4.5 * (-velx + vely) * (-velx + vely) - 1.5 * (velxx + velyy) ) ;
                src->get(x, y, Direction::NW) += omega * (feqNW - src->get(x, y, Direction::NW)) + 3.0 * Stencil::Weight::S * rho * (-ax + ay);
                
                // for the west direction
                real_t feqW  = Stencil::Weight::S * rho * (1.0 - 3.0 * velx + 4.5 * velxx - 1.5 * (velxx+ velyy)) ;
                src->get(x, y, Direction::W) += omega * (feqW - src->get(x, y, Direction::W))+ 3.0 * Stencil::Weight::S * rho * (-ax) ;
                
                // for the south west
                real_t feqSW = Stencil::Weight::D * rho * (1.0 -3.0 * (velx+ vely) + 4.5 * (velx+ vely) * (velx+ vely) - 1.5 * (velxx+ velyy) ) ;
                src->get(x, y, Direction::SW) += omega * (feqSW - src->get(x, y, Direction::SW))+ 3.0 * Stencil::Weight::S * rho * (-ax-ay) ;
                
                // for the South direction
                real_t feqS = Stencil::Weight::S * rho * (1.0 -3.0 * vely + 4.5 *velyy - 1.5 * (velxx + velyy) ) ;
                src->get(x, y, Direction::S) += omega * (feqS  - src->get(x, y, Direction::S)) + 3.0 * Stencil::Weight::S * rho * (-ay) ;
                
                // for the south east direction
                real_t feqSE = Stencil::Weight::D * rho * (1.0 + 3.0 * (velx - vely) + 4.5 * (velx-vely) * (velx-vely) - 1.5 * (velxx+ velyy) ) ;
                src->get(x, y, Direction::SE) += omega * (feqSE - src->get(x, y, Direction::SE) ) + 3.0 * Stencil::Weight::S * rho * (ax-ay)  ;
            }
        }
    }
}

void LBM::Solver::ObstacleSweep(){
    
    // Go to obstacle cell and apply ...reflective boundary condition
    
}

void LBM::Solver::SwapPointers(){
    std::swap(src,dst) ;
}

void LBM::Solver::IndentifyObstacle(const real_t dx,const real_t obsrad){
    
    const real_t offset = dx * 0.5  ;
    real_t xcord , ycord = 0.0 ;
    // Traversing through the interior cells
    for (u_int y = 1; y < celly-1; ++y) {
        for (u_int x =1; x < cellx-1; ++x) {
            // Identity distance of the current cell
            xcord = x * dx + offset ;
            ycord = y * dx + offset ;
            //std::cout<<xcord<<std::endl ;
            if (distbw(xcord, ycord, .02, 0.008) < obsrad) {
                boolarray->get(x, y, Stencil::vector::x) = 1;
            }
            else boolarray->get(x, y, Stencil::vector::x) = 0 ;
        }
    }
}

void LBM::Solver::ObsTreatment(){
    
    // Traversing the interior
    for (u_int y =1; y < celly-1; ++y) {
        for (u_int x = 1 ; x < cellx-1; ++x) {
            if (boolarray->get(x, y, Stencil::vector::x)==1) {
                
                src->get(x, y, Direction::C)  = src->get(x, y, Direction::C) ;
                src->get(x, y, Direction::E)  = src->get(x+1, y, Direction::W) ;
                src->get(x, y, Direction::NE) = src->get(x+1, y+1, Direction::SW) ;
                src->get(x, y, Direction::N)  = src->get(x, y+1, Direction::S) ;
                src->get(x, y, Direction::NW) = src->get(x-1, y+1, Direction::SE) ;
                src->get(x, y, Direction::W)  = src->get(x-1, y, Direction::E) ;
                src->get(x, y, Direction::SW) = src->get(x-1, y-1, Direction::NE) ;
                src->get(x, y, Direction::S)  = src->get(x, y-1, Direction::N) ;
                src->get(x, y, Direction::SE) = src->get(x+1, y-1, Direction::NW) ;
                
            }
        }
    }
}

const real_t LBM::Solver::distbw(const real_t x1, const real_t y1, const real_t x2, const real_t y2){
    
    real_t dist = std::sqrt( (x1-x2)*(x1-x2) + (y1-y2) * (y1 -y2)) ;
    return dist ;

}



