//
//  Enums.h
//  LBM
//
//  Created by Sagar Dolas on 30/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Enums_h
#define Enums_h

namespace LBM  {
    
    enum LatticeVariables {
        
        density = 1 ,
        velocities = 2 ,
        discretization = 9 ,
    };
    
    
    enum boundary {
        
        reflective = 0 ,
        periodic  = 1,
        
    };
    
    enum vector {
        x = 0 ,
        y = 1
    };
}

#define sqr(x) x*x


#endif /* Enums_h */
